package pl.javadevelopers.examples.controllers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import pl.javadevelopers.examples.resolver.configurations.WebMvcConfig;
import pl.javadevelopers.examples.resolver.controllers.TestController;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


/**
 * Created by 4ib3r on 08.03.15.
 * Integratin test for argument resolver in spring mvc environment.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebMvcConfig.class, TestController.class})
@WebAppConfiguration
public class TestControllerTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    /** Setup mvc mock */
    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.wac).build();
    }

    /** Perform test */
    @Test
    public void checkArgumentResolverTest() throws Exception {
        MockHttpSession session = new MockHttpSession();
        this.mockMvc.perform(get("/")
                .session(session))
                .andExpect(status().isOk())
                .andExpect(content().string("ExampleAttr{value='" + session.getId() + "'}"));
    }
}