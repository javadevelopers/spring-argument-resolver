package pl.javadevelopers.examples.resolver.dto;

/**
 * Created by 4ib3r on 08.03.15.
 * Example object to use as controller method argument.
 */
public class ExampleAttr {
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ExampleAttr{" +
                "value='" + value + '\'' +
                '}';
    }
}
