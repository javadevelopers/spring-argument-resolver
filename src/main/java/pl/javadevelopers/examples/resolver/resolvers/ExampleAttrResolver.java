package pl.javadevelopers.examples.resolver.resolvers;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import pl.javadevelopers.examples.resolver.dto.ExampleAttr;

/**
 * Created by 4ib3r on 08.03.15.
 * {@link ExampleAttr} resolver, create object and set session id as value.
 */
public class ExampleAttrResolver implements HandlerMethodArgumentResolver {

    /** Check method parameter is supported by this resolver */
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType() == ExampleAttr.class;
    }

    /** Resolve and return argument */
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        ExampleAttr attr = new ExampleAttr();
        attr.setValue(webRequest.getSessionId());
        return attr;
    }

}
