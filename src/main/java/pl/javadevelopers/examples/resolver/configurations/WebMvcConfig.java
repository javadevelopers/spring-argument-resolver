package pl.javadevelopers.examples.resolver.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import pl.javadevelopers.examples.resolver.resolvers.ExampleAttrResolver;

import java.util.List;

/**
 * Created by 4ib3r on 08.03.15.
 * Add {@link ExampleAttrResolver} to spring context.
 */
@Configuration
@EnableWebMvc
public class WebMvcConfig extends WebMvcConfigurerAdapter {
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        super.addArgumentResolvers(argumentResolvers);
        argumentResolvers.add(new ExampleAttrResolver());
    }
}
