package pl.javadevelopers.examples.resolver.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.javadevelopers.examples.resolver.dto.ExampleAttr;

/**
 * Created by 4ib3r on 08.03.15.
 * Test controller with {@link ExampleAttr}
 */
@Controller
public class TestController {

    @RequestMapping("/")
    public @ResponseBody String test(ExampleAttr exampleAttr) {
        return exampleAttr.toString();
    }
}
